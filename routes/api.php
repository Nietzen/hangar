<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api')
;

Route::group([ 'prefix' => 'song/v1' ], function () {
    Route::get('/search', 'SongController@searchAction')->name('search_song');
    Route::get('/{id}', 'SongController@getAction')->name('get_song_by_id');
    Route::post('/', 'SongController@createAction')->name('create_song');
    Route::put('/{id}', 'SongController@updateAction')->name('update_song');
    Route::delete('/{id}', 'SongController@deleteAction')->name('delete_song');
});
