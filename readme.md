LARAVEL - API
=============

**Autor:**


Jose Segura Nietzen

**Valid Content-Type:**

 * application/x-www-form-urlencoded [METHOD "PUT" IMPORTANT]
 * multipart/form-data
 * application/json

**CRUD Actions:**

 METHOD      | PATH                | ACTION        | NAME
 ----------- | ------------------- | ------------- | -------------
 GET         | /api/song/v1/search     | search        | search_song
 GET         | /api/song/v1/{id}       | search_by_id  | get_song_by_id
 POST        | /api/song/v1            | create        | create_song
 PUT         | /api/song/v1/{id}       | update        | update_song
 DELETE      | /api/song/v1/{id}       | delete        | delete_song

 **Song Attributes: **

 Attribute     | Type          | Required      | Generated
 ------------- | ------------- | ------------- | -------------
 id            | integer       | No            | Yes
 songname      | string        | Yes           | No
 artistid      | string        | Yes           | No
 artistname    | string        | Yes           | No
 url           | string        | Yes           | No
 albumid       | string        | Yes           | No
 albumname     | string        | Yes           | No
 created_at    | datetime      | No            | Yes
 updated_at    | datetime      | No            | Yes

**Examples**

**URL: api/song/v1/1**
**METHOD: GET**
**DATA:**

    null

 **RESULT**

      {
        "id": 1,
        "songname": "Engel",
        "artistid": "1234abc",
        "artistname": "Rammstein",
        "url": "youtube:x2rQzv8OWEY",
        "albumid": "1231sc",
        "albumname": "Sehnsucht",
        "created_at": "2017-01-19 06:27:37",
        "updated_at": "2017-01-10 06:27:37"
      }

 **URL: api/song/v1**
 **METHOD: POST**
 **DATA:**

     songname   :Engel
     artistid   :1234abc
     artistname :Rammstein
     url        :youtube:x2rQzv8OWEY
     albumid    :1231sc
     albumname  :Sehnsucht

 **RESULT**

     {
       "id": 1,
       "songname": "Engel",
       "artistid": "1234abc",
       "artistname": "Rammstein",
       "url": "youtube:x2rQzv8OWEY",
       "albumid": "1231sc",
       "albumname": "Sehnsucht",
       "created_at": "2017-01-19 06:27:37",
       "updated_at": "2017-01-10 06:27:37"
     }

 **URL: api/song/v1/search**
 **METHOD: GET**
 **DATA:**

     songname   :Engel
     artistid   :1234abc
     artistname :Rammstein
     url        :youtube:x2rQzv8OWEY
     albumid    :1231sc
     albumname  :Sehnsucht

 **RESULT**

     [
        {
           "id": 1,
           "songname": "Engel",
           "artistid": "1234abc",
           "artistname": "Rammstein",
           "url": "youtube:x2rQzv8OWEY",
           "albumid": "1231sc",
           "albumname": "Sehnsucht",
           "created_at": "2017-01-19 06:27:37",
           "updated_at": "2017-01-10 06:27:37"
         }
     ]

 **URL: api/song/v1/1**
 **METHOD: PUT**
 **DATA:**

     songname   :Engel
     artistid   :1234abc
     artistname :Rammstein Best
     url        :youtube:x2rQzv8OWEY
     albumid    :1231sc
     albumname  :Sehnsucht

 **RESULT**

     {
           "id": 1,
           "songname": "Engel",
           "artistid": "1234abc",
           "artistname": "Rammstein Best",
           "url": "youtube:x2rQzv8OWEY",
           "albumid": "1231sc",
           "albumname": "Sehnsucht",
           "created_at": "2017-01-19 06:27:37",
           "updated_at": "2017-01-10 06:27:37"
     }

 **URL: api/song/v1/1**
 **METHOD: DELETE**
 **DATA:**

    Null

 **RESULT**

     HTTP: 204


 **This project have voyager admin**

 User: admin@admin.com
 Password: password

 The database is in the project.

 I install voyager because have a admin of songs and test voyager.