<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;

class SongController extends Controller
{
    /**
     * @var $song Song
     */
    private $song;

    /**
     * @param $song Song
     */
    public function __construct(Song $song)
    {
        $this->song = $song;
    }

    public function getAction($id)
    {
        $song = $this->song->find($id);

        return $song ?: $this->responseError();
    }

    public function searchAction(Request $request)
    {
        $requestData = $request->all();

        $count = $this->validateFieldsHas($requestData, 0);
        if (!is_numeric($count)) {
            return $count;
        }

        if ($count < 1) {
            return $this->responseError(( 'Bad Request, I need at least 1 field. Fields to search: "' . implode('", "', $this->song->getFillable()) . '"' ), 400);
        }

        return $this->song->search($requestData);
    }

    public function createAction(Request $request)
    {
        $requestData = $request->all();

        $count = $this->validateFieldsHas($requestData, 0);
        if (!is_numeric($count)) {
            return $count;
        }

        if ($count !== count($this->song->getFillable())) {
            return $this->responseError(( 'Bad Request, missing fields "' . implode('", "', $this->song->getFillable()) . '"' ), 400);
        }

        $song = new Song($requestData);
        $song->save();

        return $song;

    }

    public function updateAction(Request $request, $id)
    {
        $song = $this->song->find($id);

        if (!$song) {
            return $this->responseError();
        }

        $requestData = $request->all();

        $count = $this->validateFieldsHas($requestData, 0);
        if (!is_numeric($count)) {
            return $count;
        }

        if ($count !== count($this->song->getFillable())) {
            return $this->responseError(( 'Bad Request, missing fields "' . implode('", "', $this->song->getFillable()) . '"' ), 400);
        }

        $song->update($requestData);

        return $song;
    }

    public function deleteAction($id)
    {
        $count = $this->song->destroy($id);

        if ($count == 0) {
            return $this->responseError();
        }

        return $this->responseError(['msn' => 'no content'], 204);
    }

    private function validateFieldsHas($requestData, $count = 0)
    {
        foreach ($requestData as $key => $item) {
            if (!in_array($key, $this->song->getFillable())) {
                return $this->responseError(( 'Bad Request, missing fields "' . implode('", "', $this->song->getFillable()) . '"' ), 400);
            }

            if (!( isset( $item ) && $item != '' )) {
                return $this->responseError('Bad Request, field: "' . $key . '" can not be empty.', 400);
            }
            $count++;
        }

        return $count;
    }


}
