<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Song
 *
 * @mixin \Eloquent
 * @property int            $id
 * @property string         $name
 * @property string         $album
 * @property string         $song
 * @property string         $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereName( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereAlbum( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereSong( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereUrl( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Song whereUpdatedAt( $value )
 */
class Song extends Model
{
    protected $table = 'song';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'songname', 'artistid', 'artistname', 'url', 'albumid', 'albumname'
    ];

    /**
     * @param $data array [ 'column1' => 'data1', 'column2' => 'data2', 'column3' => 'data3' ]
     * @return Song[]
     */
    public function search($data)
    {
        /**
         * @var $qb \Illuminate\Database\Query\Builder
         */
        $qb = DB::table($this->table);

        $where = [];

        foreach ($data as $column => $info) {
            array_push($where, [$column, 'like', '%'. $info .'%']);
        }

        $qb->where($where);

        return $qb->get();
    }
    
    

}
